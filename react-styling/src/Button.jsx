import styled from "styled-components";

export const Button = styled.button`
  background-color: ${(props) => props.color ? props.color : 'red'};
  color: white;
  font-weight: 600;
  font-size: 18px;
  border: none;
  &:hover {
    background-color: yellow;
    color: red
  }
`;