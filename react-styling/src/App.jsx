import Alert from "./components/Alert";
import styled from "styled-components";
import ButtonBlock from "./components/ButtonBlock";
import { useState } from "react";
import { Card, CardContent, Typography, CardActions, Button } from "@mui/material";
import GridUI from "./components/GridUI";



const StyledButtonBlock = styled(ButtonBlock)`
  border: 5px solid blue;
  padding: 5px;
  display: inline-block;
`


function App() {

  const [currentAlerts, setCurrentAlerts] = useState([]);

  const addNewAlert = (alertInfo) => {
    setCurrentAlerts([...currentAlerts, alertInfo])
    setTimeout(() => {
      console.log('ok')
      setCurrentAlerts((old) => {
        old.shift();
        return [...old];
      })
    }, 5000)
  }

  return (
    <div className="App">
      {currentAlerts.map(alertInfo => <Alert type={alertInfo.type}>{alertInfo.text}</Alert>)}
      <StyledButtonBlock alertTrigger={addNewAlert}/>
      <Card>
        <CardContent>
        <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
          Word of the Day
        </Typography>

        <CardActions>
          <Button size="small">Learn More</Button>
          <Button size="small">Learn More</Button>
          <Button size="small">Learn More</Button>
        </CardActions>
        </CardContent>
      </Card>
      <GridUI/>
    </div>
  );
}

export default App;
