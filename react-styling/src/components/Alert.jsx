import styled from "styled-components";

const Alert = styled.p`
    margin: 15px 0;
    background-color: ${({type}) => type==='default' ? 'lightgreen' : type==='warning' ? 'yellow' : type==='error' ? 'red' : 'lightgreen'};
    border: 2px solid lightgreen;
    border-radius: 5px;
    padding: 15px;
    font-family: 'Courier New', Courier, monospace;
    font-size: 18px;
    letter-spacing: 1.1;
    font-weight: 600;
    color: ${({type}) => type==='default' ? 'green' : type==='warning' ? 'red' : type==='error' ? 'white' : 'green'};
`

export default Alert;