import { Button, Container } from "@mui/material";

function ButtonBlock({className, alertTrigger}) {
    return ( 
        <Container className={className}>
            <Button variant="contained" color="success" onClick={() => alertTrigger({type: 'default', text: 'hello from trigger'})}>Default</Button>
            <Button variant="contained" onClick={() => alertTrigger({type: 'warning', text: 'warning in trigger'})}>Warning</Button>
            <Button variant="contained" color="error" onClick={() => alertTrigger({type: 'error', text: 'error in trigger'})}>Error</Button>
        </Container>
     );
}

export default ButtonBlock;