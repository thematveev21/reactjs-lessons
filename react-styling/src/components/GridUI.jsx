import { Grid, Card, CardContent, CardHeader } from "@mui/material";

function GridUI() {
    return ( 
        <Grid container spacing={10}>
            <Grid item>
                <Card>
                    <CardContent>
                        Content
                    </CardContent>
                </Card>
            </Grid>
            <Grid item>
            <Card>
                    <CardContent>
                        Content
                    </CardContent>
                </Card>
            </Grid>
            <Grid item>
            <Card>
                    <CardHeader title="This is header" />
                    <CardContent>
                        
                        
                    </CardContent>
                </Card>
            </Grid>
            <Grid item>
            <Card>
                    <CardContent>
                        Content
                    </CardContent>
                </Card>
            </Grid>
            <Grid item>
            <Card>
                    <CardContent>
                        Content
                    </CardContent>
                </Card>
            </Grid>
            <Grid item>
            <Card>
                    <CardContent>
                        Content
                    </CardContent>
                </Card>
            </Grid>
        </Grid>
     );
}

export default GridUI;