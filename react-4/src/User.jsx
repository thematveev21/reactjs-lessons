import PropTypes from 'prop-types'
import styles from './styles/User.module.css'

function User({firstname, lastname, phone}){




    return (
        <li className={styles.item}>
            <span className={styles.name}>{firstname + " " + lastname}</span>
            <span className={styles.phone}>{phone}</span>
        </li>
    )
}


User.propTypes = {
    firstname: PropTypes.string.isRequired,
    lastname: PropTypes.string.isRequired,
    phone: PropTypes.string.isRequired
}

export default User