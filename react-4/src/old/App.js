import { useState } from "react";


function App() {

  // hook useState
  const [name, setName] = useState("Maksim");
 
  function changeNameHandler(){
    setName(prev => prev + ".")
  }

  return (
    <div className="App">
      <h1>Name of user: {name}</h1>
      <button onClick={changeNameHandler}>Change name</button>
    </div>
  );
}

export default App;
