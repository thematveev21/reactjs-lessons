import { useState, useEffect } from "react";


function App() {

  const [number, setNumber] = useState(0) 

  useEffect(() => {
    console.log("Mount or update (re-render)")
    
  })

  useEffect(() => {
    console.log("Only mount")
    return () => {
      console.log("Unmount")
    }
  }, [])

  return (
    <div className="App">
      {number}
      <button onClick={() => setNumber(prev => prev + 1)}>Increase</button>
    </div>
  );
}

export default App;
