import { useEffect, useState } from 'react';
import User from './User'
import Form from './Form';
import styles from './styles/List.module.css'


function App() {

  const [users, setUsers] = useState([])

  console.log("render")


  useEffect(() => {
    const url = "https://dummyjson.com/users"
    fetch(url).then(resp => resp.json()).then(data => setUsers(data.users))
  }, [])


  const addNewUser = (userInfo) => {
    setUsers(prev => {
      return [userInfo, ...prev]
    })
  }
  


  return (
    <div>
      <Form addUserHandler={addNewUser}/>
       <ul className={styles.list}>
        {users.map(u => <User firstname={u.firstName} lastname={u.lastName} phone={u.phone} key={u.id}/>)}
      </ul>
    </div>
   
  );
}

export default App;
