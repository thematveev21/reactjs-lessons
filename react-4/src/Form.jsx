import { useState } from "react"


function Form({addUserHandler}){
    const [data, setData] = useState({
        firstName: "",
        lastName: "",
        phone: ""
    })


    return (
        <form>
            <input
                type="text"
                placeholder="firstname"
                onInput={event => setData(prev => {return {...prev, firstName: event.target.value}})}
            />

            <input
                type="text"
                placeholder="lastname"
                onInput={event => setData(prev => {return {...prev, lastName: event.target.value}})}
            />

            <input
                type="text"
                placeholder="phone"
                onInput={event => setData(prev => {return {...prev, phone: event.target.value}})}
            />

            <button onClick={(event) => {addUserHandler(data); event.preventDefault()}}>Add</button>
        </form>
    )
}


export default Form