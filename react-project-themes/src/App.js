import { useState } from "react";
import ThemeContext from "./ThemeContext";
import MainPage from "./MainPage";

function App() {

  const [theme, setTheme] = useState(false)

  return (
    <ThemeContext.Provider value={theme}>
      <div className="App" style={{background: theme ? 'darkblue' : 'white'}}>
        <input type="checkbox" checked={theme} onChange={() => setTheme(!theme)}/>
        <MainPage />
      </div>
    </ThemeContext.Provider>
    
  );
}

export default App;
