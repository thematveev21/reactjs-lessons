import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css'
import App from './App';
import GlobalContext from './GlobalContext';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <GlobalContext.Provider value={"this is text from global"}>
      <App />
    </GlobalContext.Provider>
);

