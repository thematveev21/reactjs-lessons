import ThemeContext from "./ThemeContext";
import { useContext } from "react";
import Article from "./Article";

function MainPage() {

    const theme = useContext(ThemeContext)

    const styles = {
        height: '100vh',
        width: '100vw',
        color: theme ? 'white' : 'black'
    }

    return ( 
        <div style={styles}>
            <h1>Hello world | CurrentTheme: {theme ? "dark" : "light"}</h1>
            <Article />
        </div>
     );
}

export default MainPage;