import { useContext } from "react";
import ThemeContext from "./ThemeContext";
import GlobalContext from "./GlobalContext";

function Article() {

    const theme = useContext(ThemeContext)
    const data = useContext(GlobalContext)

    return ( 
        <article style={{textDecoration: theme ? null : 'underline'}}>
            <h2>{data}</h2>
            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Animi sequi mollitia cumque, corporis, blanditiis eos suscipit reiciendis optio voluptates consectetur unde debitis sapiente alias earum illum tempore adipisci ipsa corrupti, asperiores nam placeat perferendis velit voluptas nesciunt. Omnis architecto qui officiis, necessitatibus aspernatur hic facilis cum sit sed tenetur provident perspiciatis voluptatibus laboriosam! Rem deserunt inventore quisquam odio cumque nesciunt ipsam, ex, dolorum soluta autem, eos asperiores sed dolorem laboriosam illo quod nam vitae reprehenderit magnam voluptatum dolore maxime vero? Maxime, hic aut incidunt repellendus ducimus nisi voluptatum perspiciatis, earum ea soluta nostrum nemo obcaecati voluptates aspernatur et commodi libero totam nulla exercitationem eaque. Perferendis, ipsa doloremque, eum quis quos dicta voluptates vitae pariatur repellendus veritatis eaque, nobis suscipit. Distinctio perferendis earum, atque dignissimos nulla velit dolores cum? Unde quasi odio eveniet odit architecto. Non saepe sed esse fugit illo numquam assumenda! Iste nisi, suscipit vel odit itaque unde distinctio ipsa porro quis? Sunt ab, tenetur quia molestiae reprehenderit doloremque odio maxime vitae itaque. Quae, amet. Vero architecto ipsa sunt earum quod dolores eligendi, quam facilis qui illum quisquam eum odit velit voluptatibus minus distinctio excepturi ducimus necessitatibus accusamus quae numquam expedita impedit nisi. Aliquid exercitationem impedit placeat! Ea labore veritatis tenetur praesentium reprehenderit cumque incidunt quis aliquid saepe molestiae quaerat, dolorem exercitationem dicta quae sed dolore voluptatibus error iure quod. Fuga iste doloribus beatae eum magni quia provident, deserunt natus sunt, porro quidem! Repellat accusantium temporibus ea ipsa earum sint laborum maiores officiis odio recusandae nostrum possimus totam fugit, debitis nulla suscipit qui sed repudiandae! Doloribus ex ad sapiente nam obcaecati tempora sed atque assumenda exercitationem! Architecto sequi cumque accusantium dicta ea eum dolores porro ullam delectus error repellendus alias soluta beatae sapiente ducimus, possimus nesciunt placeat eligendi? Sunt in architecto nostrum enim consectetur explicabo, aspernatur neque sed? Natus?
        </article>
     );
}

export default Article;