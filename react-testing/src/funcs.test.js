import {summa, DEFAULT_STATE, getProduct} from "./funcs";

const DEFAULT_PRODUCT_DATA = {
    "id": 1,
    "title": "iPhone 9",
    "description": "An apple mobile which is nothing like apple",
    "price": 549,
    "discountPercentage": 12.96,
    "rating": 4.69,
    "stock": 94,
    "brand": "Apple",
    "category": "smartphones",
    "thumbnail": "https://i.dummyjson.com/data/products/1/thumbnail.jpg",
    "images": [
    "https://i.dummyjson.com/data/products/1/1.jpg",
    "https://i.dummyjson.com/data/products/1/2.jpg",
    "https://i.dummyjson.com/data/products/1/3.jpg",
    "https://i.dummyjson.com/data/products/1/4.jpg",
    "https://i.dummyjson.com/data/products/1/thumbnail.jpg"
    ]
}





describe('my tests', () => {

    test('test summa function basic', () => {
        const r = summa(2, 2)
        expect(r).toBe(4)
        expect(summa(1, 2)).toBe(4)
    })
    
    test('test summa equal 0', () => {
        expect(summa(0, 0)).toBe(null)
    })
    
    
    test('test default state', () => {
        expect(DEFAULT_STATE).toEqual({
            name: 'Alex',
            status: undefined
        })
    })
    
    test('fetch product test', async () => {
        const data = await getProduct()
        expect(Object.keys(data)).toEqual(Object.keys(DEFAULT_PRODUCT_DATA))
    })
    



})