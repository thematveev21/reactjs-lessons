
function App() {


  const onClickHandler = () => {
    console.log("This is handler! tes")
  }

  return (
    <div className="App">
      <div>Hello</div>
      <button onClick={onClickHandler}>Hello btn</button>
      <p>Hello</p>
      <button></button>
    </div>
  );
}

export default App;
