export const DEFAULT_STATE = {
    name: 'Alex',
    status: undefined
}


export const VALUES = [1, 2, 3, 2, 1, 1, 4, 3]


export function summa(a, b){
    if (a === 0 && b === 0){
        return null
    }
    return a + b
}


export function filter_array(array){
    const r = array.filter(value => value <= 100)

    r.push(0)
    r.unshift(0)

    return r
}


// async

export function getProduct(){
    return fetch('https://dummyjson.com/products/5')
        .then(res => res.json())
}