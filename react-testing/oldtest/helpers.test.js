import {run_two_times, simpleFunction} from './arrayHelpers'

const fakeCallBack = jest.fn(x => x)

test('callback test', () => {
    run_two_times(fakeCallBack)
    expect(fakeCallBack.mock.calls).toHaveLength(2)
    for (let params of fakeCallBack.mock.calls){
        expect(params[0]).toMatch(/hello/)
    }
})


test('test snapshot', () => {
    const r = simpleFunction()
    expect(r).toMatchSnapshot()
})