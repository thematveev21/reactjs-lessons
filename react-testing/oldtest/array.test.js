import { filter_array, VALUES } from "../src/funcs";


test('filter func filtering', () => {
    const values = [1, 123, 34, 254]
    expect(filter_array(values)).toEqual([0, 1, 34, 0])
})


test('filter func zeros', () => {
    const values = [1, 123, 34, 254, 5, 4, 3]
    const r = filter_array(values)

    expect(r[0]).toBe(0)
    expect(r[r.length - 1]).toBe(0)
})


test('test var VALUES not eq to zero', () => {
    VALUES.forEach(v => expect(v).not.toBe(0))
})