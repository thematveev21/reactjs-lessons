import React, { useState } from 'react';
import ReactDOM from 'react-dom/client';
import Menu from './Menu';
import MenusContext from './MenuContext';





function Dialog(){

  const [history, setHistory] = useState([])


  function addToHistory(btnNum){
    setHistory(prev => [...prev, btnNum])
  }


  const contextData = {
    addToHistory
  }
  
  return (
    <div>
      <MenusContext.Provider value={contextData}>
        <Menu/>
        <Menu/>
      </MenusContext.Provider>

      <p>{history}</p>
    </div>
  )

}




const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <Dialog />
);


