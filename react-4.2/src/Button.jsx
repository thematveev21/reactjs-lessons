import { useContext } from "react"
import MenusContext from "./MenuContext"

function Button(props){

    const {addToHistory} = useContext(MenusContext)

    return <button style={{padding: "15px"}} onClick={(event) => addToHistory(event.target.textContent)}>{props.children}</button>
}


export default Button