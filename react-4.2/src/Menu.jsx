import Button from "./Button"

function Menu(){

    const styles = {
        border: "2px solid red",
        padding: "10px"
    }

    return (
        <div style={styles}>
            <Button>1</Button>
            <Button>2</Button>
            <Button>3</Button>
            <Button>4</Button>
            <Button>5</Button>
            <Button>6</Button>
        </div>
    )   
}


export default Menu