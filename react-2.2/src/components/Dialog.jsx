import styles from '../styles/Dialog.module.css'

function Dialog({title, content}){


    return (
        <div className={styles.dialog}>
            <h2 className={styles.title}>{title}</h2>
            <hr />
            <p className={styles.text}>{content}</p>
        </div>
    )
}


export default Dialog