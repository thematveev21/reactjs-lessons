function FullScreenWrapper({children, bg}){

    const styles = {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: bg,
        height: "100vh",
        width: "100vw"
    }

    return <div style={styles}>{children}</div>
}


export default FullScreenWrapper