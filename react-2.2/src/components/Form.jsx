import { useState } from 'react'
import styles from '../styles/Form.module.css'
import Dialog from './Dialog'

function FeedbackForm(){
    // const [formData, setFormData] = useState({})
    const [username, setUsername] = useState("")
    const [email, setEmail] = useState("")
    const [text, setText] = useState("")
    const [error, setError] = useState()


    const usernameHandler = (event) => {
        setUsername(event.target.value)
    }

    const emailHandler = (event) => {
        setEmail(event.target.value)
    }

    const textHandler = (event) => {
        setText(event.target.value)
    }


    const submitHandler = (event) => {
        event.preventDefault()

        if (username && email && text) {
            const data = {
                username,
                email,
                text
            }
            console.log(data)
        }
        else {
            setError({
                title: "Error..",
                content: "Empty fields"
            })
        }

        
    }


    return (
        <>
            {error ? <Dialog title={error.title} content={error.content}/> : null}
            <form className={styles.form}>
                <h1 className={styles.h1}>Feedback</h1>

                <input 
                    type="text" 
                    name="name" 
                    placeholder="Username" 
                    className={styles.input}
                    onInput={usernameHandler}
                />

                <input 
                    type="email"
                    placeholder="email"
                    name="email"
                    className={styles.input}
                    onInput={emailHandler}
                />

                <textarea
                    name="feed"
                    className={styles.input}
                    placeholder='your text here'
                    style={{height: "300px"}}
                    onInput={textHandler}
                ></textarea>

                <button
                    className={styles.button}
                    onClick={submitHandler}
                >Send feedback</button>
            </form>
        </>
    )
}

export default FeedbackForm