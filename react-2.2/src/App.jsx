import FullScreenWrapper from "./components/FullScreenWrapper"
import FeedbackForm from "./components/Form"

function App(){
    return (
        <>
            <FullScreenWrapper bg="lightgreen">
                <FeedbackForm />
            </FullScreenWrapper>
        </>
    )
}


export default App