import Header from "./components/header/header";
import Search from "./pages/searchPage/search";
import Favorites from "./pages/favPage/favorites";
import { Routes, Route } from "react-router";

function App() {
  return (
    <>
      <Header />
      <Routes>
        <Route path="/">
          <Route index element={<Search />}/>
          <Route path="favorites" element={< Favorites />}/>
        </Route>
      </Routes>
    </>
  );
}

export default App;
