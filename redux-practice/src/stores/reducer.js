import { ADD_TO_FAVORITES, REMOVE_FROM_FAVORITES } from "./actions"

const item = localStorage.getItem('films')

const defaultState = item ? JSON.parse(item) :  {favorites: []}

function reducer(state=defaultState, action){
    let newState;
    switch(action.type){
        case ADD_TO_FAVORITES:
            newState = {...state, favorites: [...state.favorites, action.payload]}
            localStorage.setItem('films', JSON.stringify(newState))
            return newState
        case REMOVE_FROM_FAVORITES:
            const newFavs = state.favorites.filter(film => action.payload['imdbID'] !== film['imdbID'])
            newState = {...state, favorites: [...newFavs]}
            localStorage.setItem('films', JSON.stringify(newState))
            return newState
        default:
            return state
    }
}

export default reducer