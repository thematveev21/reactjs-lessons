import styles from './header.module.css'
import { Link } from 'react-router-dom';

function Header() {
    return (
        <header className={styles.header}>
            <nav>
                <ul className={styles.ul}>
                    <li className={styles.li}>
                        <Link to="/" className={styles.a}>Search</Link>
                    </li>
                    <li className={styles.li}>
                        <Link to="/favorites" className={styles.a}>Favorites</Link>
                    </li>
                </ul>
            </nav>
        </header>
    );
}

export default Header;