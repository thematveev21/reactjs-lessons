import styles from './search.module.css'
import FilmCard from '../../components/filmCard/FilmCard';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {ADD_TO_FAVORITES} from '../../stores/actions'

function Search() {

    const [inputValue, setInputValue] = useState("")
    const [films, setFilms] = useState([])
    const dispatch = useDispatch()
    const favorites = useSelector(state => state.favorites)
    const favIds = favorites.map(f => f['imdbID'])
    console.log(favIds)


    const addToFavs = (filmInfo) => {
        dispatch({
            type: ADD_TO_FAVORITES,
            payload: filmInfo
        })
    }


    const onSearchSubmit = (event) => {
        event.preventDefault()
        const url = `http://www.omdbapi.com/?apikey=7d31d416&s=${inputValue}`
        fetch(url)
            .then(res => {
                return res.json()
            })
            .then(data => {
                if ("Search" in data){
                    setFilms(data['Search'])
                }
                
            })

    }


    return ( 
        <div className={styles.search}>
            <h1 className={styles.title}>
                Search your favorite film
            </h1>


            <form className={styles.form} onSubmit={onSearchSubmit}>

                <input
                    type="text"
                    className={styles.input}
                    placeholder='Enter film name'
                    onInput={(event) => setInputValue(event.target.value)}
                />

                <button type="submit" className={styles.btn}>Search</button>
            </form>


            <div className={styles.results}>
                {films.map(info => <FilmCard 
                                        title={info['Title']}
                                        year={info['Year']} 
                                        poster={info['Poster']}
                                        key={info['imdbID']}
                                        cardType={favIds.includes(info['imdbID']) ? 'selected' : 'notselected'}
                                        fhandler={() => addToFavs(info)}
                                    />
                                    )}
            </div>
        </div>
     );
}

export default Search;