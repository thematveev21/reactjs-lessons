import FilmCard from "../../components/filmCard/FilmCard";
import { useDispatch, useSelector } from "react-redux";
import { REMOVE_FROM_FAVORITES } from "../../stores/actions";

function Favorites() {

    const favorites = useSelector(state => state.favorites)
    const dispatch = useDispatch()
    
    const removeHandler = (filmInfo) => {
        dispatch({
            type: REMOVE_FROM_FAVORITES,
            payload: filmInfo
        })
    }

    return ( 
        <div>
            {favorites.map(info => <FilmCard 
                                        title={info['Title']}
                                        year={info['Year']} 
                                        poster={info['Poster']}
                                        key={info['imdbID']}
                                        cardType="selected"
                                        rhandler={() => removeHandler(info)}
                                    />
                                    )}
        </div>
     );
}

export default Favorites;