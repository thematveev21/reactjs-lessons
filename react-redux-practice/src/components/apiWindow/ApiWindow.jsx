import { useDispatch, useSelector } from 'react-redux';
import styles from './api.module.css'
import { useState } from 'react';

function ApiWindow() {

    const state = useSelector(state => state)
    const dispatch = useDispatch()


    const [input, setInput] = useState("")

    const sendRequestHandler = (event) => {
        event.preventDefault(); 
        fetch(input)
            .then(response => {
                
                dispatch({
                    type: "SET_STATUS",
                    payload: response.status
                })

                dispatch({
                    type: "SET_ENDPOINT",
                    payload: response.url
                })

                return response.json()

            })
            .then(data => {
                console.log(data)
                dispatch({
                    type: "SET_DATA",
                    payload: data
                })
            })
            .catch(error => {
                dispatch({
                    type: "SET_STATUS",
                    payload: "Error!"
                })
            })
    }


    return ( 
        <div className={styles.window}>
            <p className={styles.title}>Endpoint: {state.endpoint} | Status: {state.status}</p>
            <form className={styles.form}>
                <input className={styles.input} type="text" placeholder="Enter endpoint here" onInput={(event) => setInput(event.target.value)}/>
                <button className={styles.button} onClick={sendRequestHandler}> Send request</button>
            </form>
            <textarea className={styles.text} value={JSON.stringify(state.responseData)}>
                
            </textarea>
        </div>
     );
}

export default ApiWindow;