import {createStore} from 'redux'

const defaultState = {
    endpoint: "No endpoint",
    status: "No status",
    responseData: ""
}

function reducer(state = defaultState, action){
    console.log(action)
    switch(action.type){
        case "SET_ENDPOINT":
            return {...state, endpoint: action.payload}
        case "SET_STATUS":
            return {...state, status: action.payload}
        case "SET_DATA":
            return {...state, responseData: action.payload}
        default:
            return state
    }
}


const storage = createStore(reducer)


export default storage;