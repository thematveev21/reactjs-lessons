import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter, Routes, Route, Link, Outlet, useNavigate} from 'react-router-dom';


function Layout(){

  const nav = useNavigate()

  return (
    <div>
      <h1>Basic layout</h1>
      <hr />
        <Outlet />
      <hr />
      <h2>Under layout</h2>
      <button onClick={() => nav(-1)}>Back</button>
      <button onClick={() => nav("/users")}>Users</button>
    </div>
  )
}

function App() {
  return ( 
    <div className="app">
      <Routes>
        <Route path="/" element={<Layout />}>
            <Route path='messages' element={
              <h1>
                This si messages
              </h1>
            }/>
            <Route path='users' element={<h1>This is users</h1>}/>
            <Route path='posts' element={<h1>This is posts</h1>}/>
        </Route>


      </Routes>
    </div>
   );
}


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <BrowserRouter>
    <App />
  </BrowserRouter>

);


