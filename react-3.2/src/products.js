const products = [
    {id: 1, name: "iPhone 12", price: 780},
    {id: 2, name: "TV", price: 1400},
    {id: 3, name: "Player", price: 100},
    {id: 4, name: "Playstation", price: 400},
    {id: 5, name: "Headphones", price: 200},
    {id: 6, name: "Macbook", price: 2000},
    {id: 7, name: "PC", price: 1000},
    {id: 8, name: "AirPods", price: 60},
]


export default products