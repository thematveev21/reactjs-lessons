import styles from './styles/Product.module.css'
import PropTypes from 'prop-types'


function Product(props){


    return (
        <div className={styles.card}>
            <button className={styles.deleteButton} onClick={() => props.deleteHandler(props.id)}>X</button>
            <h2>{props.name}</h2>
            <p>{props.price} $</p>
            <button className={styles.button}>Buy</button>
        </div>
    )
}





Product.propTypes = {
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,

    deleteHandler: PropTypes.func.isRequired
}


export default Product