import products from "./products";
import Product from './Product'
import { useState } from "react";


function App() {

  const styles = {
    display: 'flex',
    flexWrap: 'wrap'

  }

  const [prods, setProds] = useState(products)

  const removeProduct = (id) => {
    setProds(prods.filter(p => p.id !== id))
  }

  const productsElements = prods.map(p => 
    <Product 
      key={p.id}
      id={p.id}
      name={p.name}
      price={p.price}
      deleteHandler={removeProduct}
    />
  )

  return (
    <div className="products" style={styles}>
      {productsElements}
    </div>
  );
}

export default App;
