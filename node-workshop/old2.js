// my server v1.0

const http = require('http')
const fs = require('fs')

// const server = http.createServer(function(req, res){
//     console.log('Request detected!')
//     console.log(req);

//     res.write('Hello from server!')
//     res.end()
// })



// html server

// reading of html template

const markup = fs.readFileSync('./templates/page.html').toString()


const server = http.createServer((req,res) => {

    console.log(req.url)
    res.write(markup)
    res.end()
})


server.listen(3000)