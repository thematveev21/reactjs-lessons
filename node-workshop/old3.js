const events = require('events')
const fs = require('fs')

const eventEmitter = new events.EventEmitter()


eventEmitter.on('log', (data) => {
    fs.appendFile('log.txt', data, () => {
        console.log('OK')
    })
})



eventEmitter.emit('log', 'hello')
eventEmitter.emit('log', 'hello1')
eventEmitter.emit('log', 'hello2')


