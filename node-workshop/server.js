const http = require('http')

const PORT = 3000;


const users = [
    {
        name: 'Alex',
        age: 34
    },
    {
        name: 'Viktor',
        age: 33
    },
    {
        name: 'Andrew',
        age: 22
    },
    {
        name: 'Maksim',
        age: 28
    }
]


const cars = ['BMW', 'AUDI', 'KIA', 'LEXUS', 'LANOS']

const usersReqExp = new RegExp(/\/users\/[0-9]+/)

const server = http.createServer((req, res) => {
    console.log(req.url)
    if (req.url === '/users'){
        res.write(JSON.stringify(users))
    }
    else if (req.url === '/cars'){
        res.write(JSON.stringify(cars))
    }
    else if (usersReqExp.exec(req.url)){
        const index = Number(req.url.split('/').pop())
        console.log(index)
        res.write(JSON.stringify(users[index]))
    }
    else {
        res.write("Hello world!")
    }


    res.end()
    
})

server.listen(PORT)