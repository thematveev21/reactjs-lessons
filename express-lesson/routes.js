const fs = require('fs/promises')

function mainEndpoint(request, response){
    const data = request.body
    data.checked = true
    response.send(data)
}


async function counter(request, response){
    const PATH = './assets/counter.txt'

    const buffer = await fs.readFile(PATH)
    const data = buffer.toString()
    const newCounterValue = Number(data) + 1
    await fs.writeFile(PATH, newCounterValue.toString())
    response.send({counter: newCounterValue})
}




// file handler
async function uploader(request, response){
    // const filenames = [];
    // for (let file of request.files){
    //     filenames.push(file.originalname);
    //     await fs.rename(file.path, 'uploads/' + file.originalname)
    // }
    // response.send(filenames)

    const file = request.files[0]
    const content = await fs.readFile(file.path)
    await fs.unlink(file.path)
    response.send(content.toString())
}


module.exports = {
    mainEndpoint,
    counter, uploader
}