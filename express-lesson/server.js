const express = require('express')
const {mainEndpoint, counter, uploader} = require('./routes')
const multer = require('multer')


const app = express()
const upload = multer({dest: './uploads/'})

app.use(express.json())
app.use(express.static('./templates'))

app.get('/', mainEndpoint)
app.get('/counter', counter)
app.post('/upload_file', upload.array('files'), uploader)



app.listen(3000)