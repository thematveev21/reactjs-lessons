import { useState } from 'react'
import styles from './Slider.module.css'


function Slider({urls}){

    

    const [value, setValue] = useState(0)

    const changeImageHandler = () => {
        if (urls.length - 1 > value){
            setValue(value + 1)
        }
        else {
            setValue(0)
        }
        
    }

    return (
        <div className={styles.slider}>
            {urls.length === 0 ? 
            <h1>No images here</h1> : 
            <img src={urls[value]} alt="" className={styles.img} onClick={changeImageHandler}/>
            }
            
        </div>
    )
}


export default Slider