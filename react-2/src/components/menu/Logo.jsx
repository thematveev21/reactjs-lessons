import logoImage from '../../logo.png'


function Logo(){

    const styles = {
        width: '40px',
    }


    return <img src={logoImage} alt="logo" style={styles}/>
}


export default Logo