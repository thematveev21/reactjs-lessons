import styles from './MenuItem.module.css'

function MenuItem(props){

    return (
        <li>
            <a href="#" className={styles.menuLink}>
                {props.children}
            </a>
        </li>
    )
}

export default MenuItem