import styles from './Menu.module.css'
import MenuItem from './MenuItem'
import Logo from './Logo'

// menu component


function Menu(){

    const menuLinks = ["Home", "About", "Contacts", "Team", "Map", "Products"]

    return (
        <nav className={styles.menuWrapper}>
            <Logo />
            <ul className={styles.menuList}>
                {menuLinks.map((item, index) => <MenuItem key={index}>{item}</MenuItem>)}
            </ul>
        </nav>
    )
}


export default Menu