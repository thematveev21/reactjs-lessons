import Menu from "./components/menu/Menu"
import Slider from "./components/offer/Slider"

function App(){

    const cars_images = [
        "https://carwow-uk-wp-3.imgix.net/18015-MC20BluInfinito-scaled-e1666008987698.jpg",
        "https://cdn.jdpower.com/Average%20Weight%20Of%20A%20Car.jpg",
        "https://images.unsplash.com/photo-1494976388531-d1058494cdd8?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8M3x8Y2Fyc3xlbnwwfHwwfHx8MA%3D%3D&w=1000&q=80",
        "https://hips.hearstapps.com/hmg-prod/images/2024-lamborghini-revuelto-127-641a1d518802b.jpg?crop=0.813xw:0.721xh;0.0994xw,0.128xh&resize=1200:*"
    ]


    const nature_images = [
        "https://cdn.pixabay.com/photo/2018/01/14/23/12/nature-3082832_640.jpg",
        "https://img.freepik.com/free-photo/forest-landscape_71767-127.jpg",
        "https://wallpapers.com/images/hd/nature-landscape-hd-usqznq19dscdjkf8.jpg"
    ]

    return (
        <>
            <Menu />
            <Slider urls={cars_images}/>
            <Slider urls={nature_images} />
            <Slider urls={[]}/>
        
        </>
    )
}


export default App