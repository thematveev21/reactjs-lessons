import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import './index.css'


// function MyComponent(){
//   return (
//     <h1 className='title'>Hello world from react!</h1>
//   )
// }


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <App />
);


