import { useState } from 'react';
import Product from './components/Product';

const pr = [
  1, 2, 3
]


function App() {

  const [products, setProducts] = useState(pr);

  const changeName = () => {
    setProducts(old => {
      return [old[0], 100, old[2]]
    })
  }

  return (
    <>
      <hr />
      <Product name={products[0]}/>
      <Product name={products[1]}/>
      <Product name={products[2]}/>
      <hr />

      <button onClick={changeName}>Change Iphone</button>
    </>

    
  );
}

export default App;
