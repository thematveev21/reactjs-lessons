import { useContext } from "react";
import { CardContext } from "./cardContext";

function CardSlider() {

    const {img} = useContext(CardContext)

    return ( 
        <img src={img} alt="photo" />
     );
}

export default CardSlider;