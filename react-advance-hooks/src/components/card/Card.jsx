import CardSlider from "./Slider";
import Text from "./TextContent";
import {CardContext} from './cardContext'



function Card(props) {
    return ( 
        <CardContext.Provider value={{...props}}>
            <div>
                <h1>{props.name}</h1>
                <CardSlider />
                <Text />
            </div>
        </CardContext.Provider>
     );
}

export default Card;