import { useContext } from "react";
import { CardContext } from "./cardContext";

function Text() {

    const {text} = useContext(CardContext)


    return ( 
        <p>{text}</p>
     );
}

export default Text