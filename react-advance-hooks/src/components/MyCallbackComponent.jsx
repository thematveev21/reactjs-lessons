import { useCallback, useState } from "react";

function CallbackComponent(props) {
    console.log("ComponentRender!")

    const btnCallback = useCallback(props.cb, [props.cb])

    return ( 
        <> 
            <button onClick={btnCallback}>Run callback</button>
        </>
    );
}

export default CallbackComponent;