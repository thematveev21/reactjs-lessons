import { useMemo } from "react";

function Product(props) {

    console.log("Component", props.name, "rendered!")

    const val = () => props.name

    const value = useMemo(val, [props.name])

    return ( 
        <p>
            {value}
        </p>
     );
}

export default Product;