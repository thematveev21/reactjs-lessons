import { useReducer, useState } from "react";


const defaultState = {
    color: 'red',
    text: 'Hello',
    background: 'yellow'
}

function reducer(state, action){
    switch (action.type) {
        case 'COLOR':
            return {...state, color: 'green'}
        case 'BG':
            return {...state, background: 'gray'}
        case 'TEXT':
            return {...state, text: 'changed text'}
        default:
            return state
    }
}


function ReduceTest() {

    const [state, dispatch] = useReducer(reducer, defaultState);


    return (
        <div>
            <div style={{color: state.color, background: state.background}}>
                {state.text}
            </div>
            <button onClick={() => dispatch({type: 'COLOR'})}>COLOR</button>
            <button onClick={() => dispatch({type: 'BG'})}>BG</button>
            <button onClick={() => dispatch({type: 'TEXT'})}>TEXT</button>
        </div>
     );
}

export default ReduceTest;