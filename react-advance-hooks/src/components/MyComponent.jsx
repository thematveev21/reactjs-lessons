import { useMemo, useState } from "react";

function getValue(){
    console.log("Get value function trigger!")
    return "This is value!"
}


function MyComponent(props) {
    console.log("Render!")
    const [data, setData] = useState(false);

    const val = useMemo(getValue, [props.val])

    return ( 
        <div>
            <p>{val}</p>
            <button onClick={() => setData((old) => !old)}>RENDER</button>
        </div>
     );
}

export default MyComponent;