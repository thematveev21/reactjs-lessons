import { useState } from "react";
import styled from "styled-components";

const StyledForm = styled.form`
    border: 1px solid gray;
    padding: 20px;
    display: flex;
    justify-content: center;
`

const Input = styled.input`
    padding: 8px;
    font-family: Arial, Helvetica, sans-serif;
    font-size: 18px;
    width: 300px;
`


function TaskForm() {

    const [inputValue, setInputValue] = useState("")


    const formSubmitHandler = (event) => {
        event.preventDefault()

        fetch('/api/tasks', {
            method: 'POST',
            body: JSON.stringify({
                taskValue: inputValue
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json()).then(data => {
            console.log(data.ok)
        })
    }

    return ( 
        <StyledForm onSubmit={formSubmitHandler}>
            <Input type="text" name="task" onInput={(event) => setInputValue(event.target.value)}/>
            <input type="submit" />
        </StyledForm>
    );
}

export default TaskForm;