import { useEffect, useState } from "react";

function TaskList() {

    const [tasks, setTasks] = useState([]);

    const updateTasks = () => {
        fetch('/api/tasks')
            .then(res => res.json())
            .then(data => setTasks(data))
    }

    useEffect(updateTasks, [])

    const tasksComponents = tasks.map((t, index) => <li key={index}>{t}</li>)

    return ( 
        <div>
            <ul>
                {tasksComponents}
            </ul>

            <button onClick={updateTasks}>Update</button>

        </div>

    );
}

export default TaskList;