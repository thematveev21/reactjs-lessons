import React from 'react';
import ReactDOM from 'react-dom/client';



// class Title extends React.Component {

//   constructor(props){
//     super(props)
//     this.state = {
//       name: "Maksim"
//     }


//     console.log("Constructor")
//   }

//   changeNameHandler = () => {
//     this.setState({name: "Viktor"})
//   }


//   componentDidMount(){
//     console.log("Mounted!")
//   }


//   // update

//   componentDidUpdate() {
//     console.log("componentDidUpdate")
//   }


//   // unmounting

//   componentWillUnmount(){
//     console.log("componentWillUnmount")
//   }

//   render(){

//     console.log("Render")

//     return (
//       <div>
//         <h1>This is title</h1>
//         <p>Props: {this.props.text} {this.state.name}</p>
//         <button onClick={this.changeNameHandler}>Change name</button>
//       </div>
//     )
//   }

// }

import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <div>
      <App />
    </div>
);

