import React from "react"


class Window extends React.Component {
    constructor(props){
        super(props)
        this.interval = setInterval(() => console.log("Updating messages..."), 1000)
        console.log("Window constructor")
    }

    componentDidMount(){
        console.log("Window component mounted...")
    }

    componentWillUnmount(){
        console.log("Window component unmounted...")
        clearInterval(this.interval)
    }

    componentDidUpdate(){
        console.log("Window component updated...")
    }

    componentDidCatch(error, info) {
        console.log(error)
        console.log(info)
    }

    render(){

        console.log("Window render method")

        const window = {
            backgroundColor: "gray",
            padding: "30px"
        }


        return (
            <div className="window" style={window} onClick={
                () => this.state.test.ok
            }> 
                <h2 className="w-title">This is window</h2>
            </div>
        )
    }
}


export default Window