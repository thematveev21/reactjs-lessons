import { useState } from "react"
import Window from "./Window"

function App(props){

    const [wstate, setWState] = useState(false)


    const toggleWindow = () => {
        setWState(!wstate)
    }


    return (
        <div className="app">
            {wstate ? <Window /> : null}
            <button onClick={toggleWindow}>Toggle</button>
        </div>
    )
}


export default App