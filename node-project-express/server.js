const express = require('express');
const {tasks} = require('./data.js')

const app = express()
const staticMiddleware = express.static('./build')
app.use(express.json())
app.use(staticMiddleware)


// routes and handlers

app.post('/api/tasks', (req, res) => {
    console.log('Data getted!')

    tasks.push(req.body.taskValue)
    // console.log(req)

    res.send(tasks)
})

app.get('/api/tasks', (req, res) => {
    res.send(tasks)
})



app.listen(3000, () => {
    console.log("Server started on 3000 port!")
})
