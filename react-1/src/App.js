// import Product from "./components/productCard/Product";


// function App() {


//   const products = ['Iphone 13', 'TV', 'Headphones'];

//   return (
//     <div>
//       {products.map((pname, index) => <Product name={pname} key={index}/>)}
//     </div>

//   );

// }

// export default App;



import Counter from "./components/counter/Counter";

function App() {

  return (
    <div>
      <Counter />
      <Counter initial={2}/>
      <Counter initial={100}/>
    </div>

  );

}

export default App;


