import styles from './Counter.module.css'
import { useState } from 'react'
import ResetButton from './ResetButton'

function Counter({initial=0}){

    const [value, setNewValue] = useState(initial)
    
    const increaseValue = () => {
        setNewValue(value + 1)
    }

    const logger = () => {
        console.log("Current state:", value)
    }

    const resetHandler = () => {
        setNewValue(0);
        console.log("Dropped!")
    }


    return (
        <div className={`${styles.counterWrapper} ${styles.counterWrapperYellow}`}>
            <h1>{value}</h1>
            <button className={styles.counterButton} onClick={() => {increaseValue();logger()}}>PLUS</button>
            <ResetButton reset={resetHandler}/>
        </div>
    )
}


export default Counter