import styles from './Counter.module.css'

function ResetButton({reset}){


    return <button className={styles.resetButton} onClick={reset}>RESET</button>
}


export default ResetButton