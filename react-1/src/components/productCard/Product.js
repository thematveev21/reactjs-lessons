import './Product.scss'


function Product({name}) {


    return (
        <div className='product-card test'>
            <h2>{name}</h2>
            <img src="https://images.macrumors.com/t/9r84bU_ZTOTrUixxwhjHUFsAvD4=/800x0/smart/article-new/2017/09/iphonexdesign.jpg?lossy" alt="" className='product-image'/>
            <button className='product-button'>Buy now</button>
        </div>
    )
}


export default Product