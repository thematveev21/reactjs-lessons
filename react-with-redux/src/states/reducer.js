import { CHANGE_NAME, CHANGE_AGE, CHANGE_CITY, UPDATE_USERLIST } from "./actions"

export const defaultState = {
    username: "Alex",
    age: 45,
    city: "London",
    userList: []
}


function defaultReducer(state = defaultState, action){
    switch (action.type) {
        case CHANGE_NAME:
            return {...state, username: action.payload}
        case CHANGE_AGE:
            return {...state, age: action.payload}
        case CHANGE_CITY:
            return {...state, city: action.payload}
        case UPDATE_USERLIST:
            return {...state, userList: [...action.payload]}
        default:
            return state
    }
}

export default defaultReducer