// action creator

export const CHANGE_NAME = "CHANGE_NAME"
export const CHANGE_AGE = "CHANGE_AGE"
export const CHANGE_CITY = "CHANGE_CITY"
export const UPDATE_USERLIST = "UPADTEUSERLIST"



export const changeName = (newName) => {
    return {
        type: CHANGE_NAME,
        payload: newName
    }
}

export const changeAge = (newAge) => {
    return {
        type: CHANGE_AGE,
        payload: newAge
    }
}

export const changeCity = (newCity) => {
    return {
        type: CHANGE_CITY,
        payload: newCity
    }
}


export const upadateUserList = (userList) => {
    return {
        type: UPDATE_USERLIST,
        payload: userList
    }
}