import defaultReducer from './reducer'
// import {createStore} from 'redux'
import { configureStore } from '@reduxjs/toolkit'


// export const store = createStore(defaultReducer) 
export const store = configureStore({
    reducer: defaultReducer
})



