// import {store} from './states/store'
import { useSelector, useDispatch} from 'react-redux'
import { changeName, changeAge, changeCity, upadateUserList } from './states/actions'
import { useEffect } from 'react'



function App(){

    const state = useSelector((state) => state)
    const dispatch = useDispatch()

    useEffect(() => {
        fetch("https://dummyjson.com/users").then(resp => resp.json()).then(data => {
            dispatch(upadateUserList(data.users))
        })
        
    }, [dispatch])

    const changeNameHandler = function(){
        dispatch(changeName("This is new action name"))
    }

    const changeAgeHandler = function(){
        dispatch(changeAge("This is new age"))
    }

    const changeCityHandler = function(){
        dispatch(changeCity("This is new city"))
    }

    return (
        <div className="app">
            <h1>Name: {state.username}</h1>
            <h1>Age: {state.age}</h1>
            <h1>City: {state.city}</h1>
            <button onClick={changeNameHandler}>reset name</button>
            <button onClick={changeAgeHandler}>reset age</button>
            <button onClick={changeCityHandler}>reset city</button>
            <ul>
                {state.userList.map(u => <li key={u.id}>{u.firstName}</li>)}
            </ul>
            
        </div>
    )
}

export default App