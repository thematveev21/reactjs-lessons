import Button from "./components/Button/Button";
import JsonContainer from "./components/JsonContainer/JsonContainer";
import Loader from "./components/Loader/Loader";

function App() {
  return (
    <div className="App">
      <h1>Hello world</h1>
      <button>Click me</button>
      <Button handler={() => console.log('Hello world!')}/>
      <JsonContainer />
      <Loader title={'Hello'}/>
    </div>
  );
}

export default App;
