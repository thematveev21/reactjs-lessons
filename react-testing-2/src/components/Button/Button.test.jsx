import { render, screen, fireEvent } from '@testing-library/react';
import Button from './Button';


const mockHandler = jest.fn()

test('button renders', () => {
    render(<Button handler={mockHandler}/>)
    const component = screen.getByText('Colored button')
    expect(component).toBeInTheDocument()
})

test('test color switch', () => {
    render(<Button handler={mockHandler}/>)
    const component = screen.getByText('Colored button')
    expect(component.style.background).toBe('yellow')
    fireEvent.click(component)
    expect(component.style.background).toBe('red')
    fireEvent.click(component)
    expect(component.style.background).toBe('yellow')
})

test('test handler works', () => {
    render(<Button handler={mockHandler}/>)
    const component = screen.getByText('Colored button')
    fireEvent.click(component)
    expect(mockHandler.mock.calls).toHaveLength(1)
    fireEvent.click(component)
    expect(mockHandler.mock.calls).toHaveLength(2)
})