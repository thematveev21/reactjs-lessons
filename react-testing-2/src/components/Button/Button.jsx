import { useState } from "react";

function Button({handler}) {
    const [clicked, setClicked] = useState(false)

    const styles = {
        backgroundColor: clicked ? 'red' : 'yellow'
    }

    const clickHandler = () => {
        setClicked(!clicked)
        handler()
    }

    return ( <button style={styles} onClick={clickHandler}>Colored button</button> );
}

export default Button;