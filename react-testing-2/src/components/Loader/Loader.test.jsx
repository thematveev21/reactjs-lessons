import {render, screen, fireEvent} from '@testing-library/react'
import Loader from './Loader'


test('check link after click', () => {
    render(<Loader />)
    const btn = screen.getByText('Click')
    const p = screen.getByText('None')

    expect(p.textContent).toBe('None')
    fireEvent.click(btn)
    expect(p.textContent).toBe('https://google.com')

    render(<Loader title={"test title"}/>)
    expect(screen.getByText('test title')).toBeInTheDocument()
})