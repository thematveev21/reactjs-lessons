import { useReducer } from "react";


// перевірити наявність компонента на сторінці
// перевірити заміну посилання після кліку
// перевірити відображення пропсу Title
//
//

const defaultState = {
    url: ''
}

export function reducer(state, action){
    switch(action.type){
        case 'CHANGE':
            return {...state, url: action.payload}
    }
}


function Loader({title}) {

    const [state,dispatch] = useReducer(reducer, defaultState)
    
    return ( 
        <div>
            <h1>{title}</h1>
            <p>{state.url ? state.url : 'None'}</p>
            <button onClick={() => dispatch({type: 'CHANGE', payload: 'https://google.com'})}>Click</button>
        </div>
     );
}

export default Loader;