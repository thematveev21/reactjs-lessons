import { useEffect, useState } from "react";

function JsonContainer() {

    const [value, setValue] = useState("")


    const fetchData = () => {
        fetch('https://dummyjson.com/carts/1')
            .then(res => res.json())
            .then(data => setValue(JSON.stringify(data)))
    }

    useEffect(() => {
        fetchData()
    }, [])

    return ( 
        <textarea value={value} readOnly></textarea>
     );
}

export default JsonContainer;