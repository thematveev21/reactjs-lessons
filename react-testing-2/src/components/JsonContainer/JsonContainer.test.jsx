import {render, screen, waitFor} from '@testing-library/react'
import JsonContainer from './JsonContainer'




// test('test request works', async () => {
//     const fn = jest.fn().mockResolvedValue({json: () => Promise.resolve({data: 'data'})})
//     global.fetch = fn

//     render(<JsonContainer />)
//     await waitFor(() => expect(screen.getByRole('textbox').value).toBe(JSON.stringify({data: 'data'})))
//     await waitFor(() => expect(fn).toHaveBeenCalled())
    
// })


test('test fetch', async () => {
    

    const fetchMock = jest.spyOn(global, 'fetch')
        .mockImplementation(() => Promise.resolve({json: () => Promise.resolve({data: 'data'})}))

    render(<JsonContainer />)
    await waitFor(() => expect(screen.getByRole('textbox').value).toBe(JSON.stringify({data: 'data'})))
    await waitFor(() => expect(fetchMock).toBeCalledTimes(1))

})