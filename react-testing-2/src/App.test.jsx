import { render, screen, cleanup } from '@testing-library/react';
import App from './App';


afterEach(cleanup)

test('renders learn react link', () => {
  render(<App />);
  const linkElement = screen.getByText(/hello world/i);
  expect(linkElement).toBeInTheDocument();

});

test('check button exists', () => {
  render(<App />)
  const comp = screen.getByText('Click me')
  
  expect(comp).toBeInTheDocument()
  expect(comp.textContent).toBe('Click me')
})
