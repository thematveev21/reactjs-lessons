import {useDispatch, useSelector} from 'react-redux'
import { SET_NAME } from '../stores/actions';
import { useState } from 'react';

function Name() {

    const state = useSelector(state => state.name)
    const dispatch = useDispatch()

    const [name, setName] = useState("")

    const formHandler = (event) => {
        event.preventDefault()
        dispatch({
            type: SET_NAME,
            payload: name
        })
    }

    return (
        <div className="name">
            <h2>Saved name: {state.savedName}</h2>
            <form onSubmit={formHandler}>
                <input type="text" placeholder="VALUE" onInput={(event) => setName(event.target.value) }/>
                <button type="submit">SET NAME</button>
            </form>
        </div>
    );
}

export default Name;