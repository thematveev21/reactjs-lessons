import { useSelector, useDispatch } from "react-redux";
import { INCREASE, DECREASE, RESET, setValue } from "./stores/actions";
import { useState } from "react";
import Name from "./components/Name";


function App() {
  const state = useSelector(state => state.counter)
  const dispatch = useDispatch()

  const [inputValue, setInputValue] = useState("")

  const formHandler = (event) => {
    event.preventDefault()
    dispatch(setValue(Number(inputValue)))
  }

  return (
    <div className="App">
      <Name />
      <h1>{state.value}</h1>
      <button onClick={() => dispatch({type: INCREASE})}>INCREASE</button>
      <button onClick={() => dispatch({type: DECREASE})}>DECREASE</button>
      <button onClick={() => dispatch({type: RESET})}>RESET</button>

      <form onSubmit={formHandler}>
        <input type="text" placeholder="VALUE" onInput={(event) => setInputValue(event.target.value)}/>
        <button type="submit">SET</button>
      </form>

    </div>
  );
}

export default App;
