import { SET_NAME } from "./actions"

const data = localStorage.getItem("savedState")

const nameDefaultState = data ? JSON.parse(data).name : {
    savedName: "No Saved Name"
}


function nameReducer(state = nameDefaultState, action) {
    switch (action.type){
        case SET_NAME:
            return { savedName: action.payload }
        default:
            return state
    }
}

export default nameReducer