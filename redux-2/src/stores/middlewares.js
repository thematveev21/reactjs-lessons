

// export function actionLogger(store) {
//     console.log("First step")
//     return function (next) {
//         console.log("Second step")
//         return function (action){
//             console.log("Start processing...")
//             console.log("store->", store)
//             console.log("next->", next)
//             console.log("action->", action)
//             console.log("End processing....")
//             next(action)
//         }
//     }
// }


export function saveToLocalStorage(state){
    return function(next){
        return function (action){
            setTimeout(() => {
                const currentState = state.getState()
                console.log(currentState)
                const data = JSON.stringify(currentState)
                localStorage.setItem("savedState", data)
                next(action)
            },)
        }
    }
}
