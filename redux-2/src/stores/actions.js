export const INCREASE = "INCREASE"
export const DECREASE = "DECREASE"
export const RESET = "RESET"
export const SET_VALUE = "SET_VALUE"


// name actions

export const SET_NAME = "SET_NAME"


// action creators

export const setValue = (newValue) => {
    return {
        type: SET_VALUE,
        payload: newValue
    }
}
    


