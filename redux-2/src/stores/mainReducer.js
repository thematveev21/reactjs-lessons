import { INCREASE, DECREASE, RESET, SET_VALUE } from "./actions"
import { combineReducers } from 'redux'
import nameReducer from './nameReducer'

const data = localStorage.getItem("savedState")

const counterDefaultState = data ? JSON.parse(data).counter : {
    value: 0,
    actions: 0
}

function counterReducer(state = counterDefaultState, action){
    // {type: ...., payload?: ....}
    switch (action.type){
        case INCREASE:
            return {value: state.value + 1, actions: state.actions + 1}
        case DECREASE:
            return {value: state.value - 1, actions: state.actions + 1}
        case RESET:
            return {value: 0, actions: state.actions + 1}
        case SET_VALUE:
            return {value: action.payload, actions: state.actions + 1}
        default:
            return state
    }
}


const rootReducer = combineReducers({
    counter: counterReducer,
    name: nameReducer
})

export default rootReducer