import { configureStore } from "@reduxjs/toolkit"
import rootReducer from "./mainReducer"
import { saveToLocalStorage } from "./middlewares"


const store = configureStore({
    reducer: rootReducer,
    middleware: [saveToLocalStorage],
    devTools: true
})


export default store