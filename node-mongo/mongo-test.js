const mongo = require('mongodb')


const uri = "mongodb://localhost:27017"
const client = new mongo.MongoClient(uri)


async function uploadData(){
    const userInfo = {
        name: 'User from js',
        age: 33,
        city: 'Kyiv'
    }

    await client.connect()

    const users = client.db('shop').collection('users')

    await users.insertOne(userInfo)
    console.log('Added!')


    client.close()

}

uploadData()




// async function getData(){
//     await client.connect()

//     const shop = client.db('shop')
//     const users = shop.collection('users')
    
//     const query = {name: 'Anna'}

//     const result = await users.findOne(query)

//     console.log(result)

//     client.close()

// }


