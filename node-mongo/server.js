const express = require('express')
const { MongoClient } = require('mongodb')

const app = express()
app.use(express.json())

const client = new MongoClient('mongodb://localhost:27017')
client.connect()
const db = client.db('backend')


app.post('/register', async (req, res) => {

    const result = await db.collection('users').find({login: req.body.login}).toArray()

    if (result.length != 0){
        res.status(409).send({result: 'user exists'})
    }
    else {
        const query = req.body
        const registerStatus = await db.collection('users').insertOne(query)
        query.id = registerStatus._id
        res.status(200).send(query)
    } 
})


app.post('/login', async (req, res) => {

    const userCredentials = await db.collection('users').findOne({login: req.body.login})
    if (userCredentials && userCredentials.password === req.body.password){
        res.status(200).send({id: userCredentials._id})
    }
    else {
        res.status(401).send({result: 'incorrect credentials'})
    }
})

app.get('/admin/users', async (req, res) => {
    const users = await db.collection('users').find().toArray()
    res.send(users)
})


app.delete('/admin/users/:login', async (req, res) => {
    const result = await db.collection('users').deleteOne({login: req.params.login})
    res.send(result)

})


app.put('/admin/users/:login', async (req, res) => {
    const result = await db.collection('users').updateOne(
        {login: req.params.login},
        {
            $set: req.body
        }
    )

    res.send(result)
})



app.listen(3000)