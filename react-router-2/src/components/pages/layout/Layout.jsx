import styles from './Layout.module.css'

function Layout(props) {

    return ( 
        <div className={styles.layout}>
            <h1 className={styles.title}>{props.title}</h1>
            {props.children}
        </div>
     );
}

export default Layout