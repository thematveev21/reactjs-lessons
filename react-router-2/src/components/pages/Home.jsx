import Layout from "./layout/Layout";

function HomeComponent() {
    return ( 
        <Layout title="Homepage">
            <h2>This is home component!</h2>
        </Layout>
     );
}

export default HomeComponent;