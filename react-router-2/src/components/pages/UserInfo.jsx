import { useEffect, useState } from "react";
import Layout from "./layout/Layout";
import { useParams } from "react-router-dom";

function UserInfo() {

    const [name, setName] = useState("")
    const params = useParams();

    useEffect(() => {
        fetch(`https://dummyjson.com/users/${params.id}`)
            .then(res => res.json())
            .then(data => setName(`${data.firstName} ${data.lastName}`))
    }, [params])


    return ( 
        <Layout title="Info page">
            {name}
        </Layout>
     );
}

export default UserInfo;  