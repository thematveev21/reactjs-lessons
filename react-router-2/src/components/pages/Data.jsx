import { useEffect, useState } from "react";
import Layout from "./layout/Layout";
import { Router, Route, Link } from "react-router-dom";


function UserCard(props){

    const styles = {
        height: "100px",
        width: "100px",
        borderRadius: "100px",
        border: "4px solid red",
        boxSizing: "content-box"
    }

    return (
        <Link to={`/data/${props.id}`}>
            <img style={styles} src={props.link} alt="photo" />
        </Link>
    )
}




function DataPage(props) {

    const [users, setUsers] = useState([])
    useEffect(() => {
        fetch("https://dummyjson.com/users")
            .then(res => res.json())
            .then(data => setUsers(data.users))
    },[])



    return ( 
        <Layout title="Data page">
            {users.map(user => <UserCard link={user.image} key={user.id} id={user.id}/>)}
        </Layout>
     );
}

export default DataPage;