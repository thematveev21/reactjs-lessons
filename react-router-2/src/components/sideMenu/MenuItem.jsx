import styles from './MenuItem.module.css'
import { Link } from 'react-router-dom';

function MenuItem(props) {
    return ( 
        <Link to={props.to} className={styles.item}>
            {props.children}
        </Link>
     );
}

export default MenuItem;