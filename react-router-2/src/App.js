import Menu from "./components/sideMenu/Menu";
import {Routes, Route} from 'react-router-dom'
import HomeComponent from "./components/pages/Home";
import DataPage from "./components/pages/Data";
import UserInfo from "./components/pages/UserInfo";

function App() {
  return (
    <div className="application">
      <Menu />
      <Routes>

        <Route path="/" element={<HomeComponent />}/>
        <Route path="/data" element={<DataPage />}/>
        <Route path="/data/:id" element={<UserInfo />}/>

        <Route path="*" element={<h1>Page under construction</h1>}/>


      </Routes>
    </div>
  );
}

export default App;
