import { applyMiddleware, createStore } from 'redux'
import thunk from 'redux-thunk'


const SET_COMMENTS = "SET_COMMENTS"
const SET_USERS = "SET_USERS"
const SET_FILTERED_COMMENTS = "SETFCOMMENTS"


const _setComments = (comments) => { 
    return {
        type: SET_COMMENTS,
        payload: comments
    }
}


export const setComments = (comments) => {
    
    return (dispatch) => {
        dispatch(_setComments(comments))
        fetch("https://jsonplaceholder.typicode.com/users")
            .then(response => response.json())
            .then(data => dispatch(setUsers(data)))
    }
}


export const setUsers = (users) => { 
    return {
        type: SET_USERS,
        payload: users
    }
}

export const setFilteredComments = (comments) => {
    return {
        type: SET_FILTERED_COMMENTS,
        payload: comments
    }
}


const initialState = {
    comments: [],
    users: [],
    filtered: []
}

function reducer(state = initialState, action){
    console.log(action)
    switch(action.type){
        case SET_COMMENTS:
            return {...state, comments: action.payload}
        case SET_USERS:
            return {...state, users: action.payload}
        case SET_FILTERED_COMMENTS:
            return {...state, filtered: action.payload}
        default:
            return state
    }
}


const store = createStore(reducer, applyMiddleware(thunk))

export default store