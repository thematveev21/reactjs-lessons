import { useDispatch, useSelector } from 'react-redux';
import CommentList from './CommentList'
import SearchForm from './SearchForm';
import { setComments } from './store';
import { useEffect } from 'react';




function App() {

  const dispatch = useDispatch()
  const users = useSelector(state => state.users)
  console.log("State users", users)
  useEffect(() => {
    fetch('https://jsonplaceholder.typicode.com/comments')
      .then(response => response.json())
      .then(data => dispatch(setComments(data)))
  }, [])


  return (
    <div>
      <SearchForm />
      <CommentList/>

      {users.map((user, index) => <p key={index}>{user.name}</p>)}
    </div>
  );
}

export default App;
