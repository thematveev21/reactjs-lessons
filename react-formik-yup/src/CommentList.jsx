import { useSelector } from "react-redux";

function CommentList() {

    const filteredComments = useSelector(state => state.filtered)


    return ( 
        <ul>
            {filteredComments.map((value, index) => <li key={index}>{value.email} | {value.body}</li>)}
        </ul>
     );
}

export default CommentList;