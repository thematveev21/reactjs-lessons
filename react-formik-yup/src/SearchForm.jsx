import { Form, Field, ErrorMessage, Formik } from 'formik'
import { useDispatch, useSelector } from 'react-redux'
import { setFilteredComments } from './store'
import { object, string } from 'yup'



const validationSchema = object().shape({
    email: string()
        .email("incorrect format")
        .min(5, "small length")
        .max(25, "big length")
        
})


function SearchForm() {

    const dispatch = useDispatch()
    const comments = useSelector(state => state.comments)

    const formSubmitHandler = (values) => {
        console.log(values)
        dispatch(setFilteredComments(comments.filter(comment => comment.email === values.email)))
    }

    return ( 
        <Formik 
            initialValues={{email: ''}}
            onSubmit={formSubmitHandler}
            validationSchema={validationSchema}
        >   
            <Form>
                <Field name="email" placeholder="Email to search"/>
                <button type='submit'>Search</button>
                <ErrorMessage name='email' component="p"/>
            </Form>
        </Formik>
     );
}

export default SearchForm;