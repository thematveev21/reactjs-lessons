import {Routes, Route} from "react-router-dom";
import HeaderMenu from "./components/header/Menu";
import UsersContainer from "./components/users/Users";
import PostsContainer from "./components/posts/Posts"
import ProductsContainer from "./components/products/ProductsContainer";

function App() {
  return (
      <>
        <HeaderMenu />
        
        <Routes>

          <Route path="/" element=""/>
          <Route path="/users" element={<UsersContainer />}/>
          <Route path="/posts" element={< PostsContainer />}/>
          <Route path="/products" element={< ProductsContainer />}/>
          

        </Routes>
      </>
  );
}

export default App;
