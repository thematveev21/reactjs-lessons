import styles from './Products.module.css'
import Product from './Product'
import { useState, useEffect } from 'react'
import axios from 'axios'


function loadDataFromLocalStorage(){
    const data = localStorage.getItem("products")
    if (data){
        return JSON.parse(data);
    }
    else return []
}



function ProductsContainer(){

    const [products, setProducts] = useState(loadDataFromLocalStorage())
    useEffect(() => {
        //axios
        if (products.length === 0){
            axios.get("https://dummyjson.com/products")
                .then(res => {
                    setProducts(res.data.products)
                    localStorage.setItem("products", JSON.stringify(res.data.products))
                })
        }
        
    },[products.length])



    return (
        <div className={styles.container}>
            {products.map(pr => <Product key={pr.id} name={pr.title} price={pr.price} image={pr.images[0]}/>)}
            

        </div>
    )
}

export default ProductsContainer