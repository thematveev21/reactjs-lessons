import styles from './Products.module.css'

function Product({name, price, image}) {

    return (
        <div className={styles.product}>
            <img src={image} alt="" className={styles.photo} />
            <div className={styles.info}>
                <h3 className={styles.name}>{name}</h3>
                <p className={styles.price}>{price} $</p>
            </div>
        </div>
    )
}

export default Product