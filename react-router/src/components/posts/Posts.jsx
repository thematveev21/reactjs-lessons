import styles from "./Posts.module.css"
import Post from "./PostItem"
import { useEffect, useState } from "react"
import axios from "axios"


function PostsContainer(){

    const [posts, setPosts] = useState([])
    useEffect(() => {
        // axios
        axios.get("https://dummyjson.com/posts")
            .then(res => {
                setPosts(res.data.posts)
            })

    },[])


    return (
        <div className={styles.container}>
            {posts.map(post => <Post text={post.body} key={post.id}/>)}
        </div>
    )
}

export default PostsContainer