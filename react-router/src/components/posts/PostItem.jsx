import styles from "./Posts.module.css"


function Post({text}){


    return (
        <p className={styles.post}>{text}</p>
    )
}

export default Post