import styles from './Menu.module.css'
import { Link } from 'react-router-dom'

function HeaderMenu(){


    return (
        <div className={styles.menu}>
            <Link to="/users" className={styles.item}>Users</Link>
            <Link to="/posts" className={styles.item}>Posts</Link>
            <Link to="/products" className={styles.item}>Products</Link>
        </div>
    )
}

export default HeaderMenu