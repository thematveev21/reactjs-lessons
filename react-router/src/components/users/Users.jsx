import styles from './Users.module.css'
import UserCard from './UserCard';
import { useEffect, useState } from 'react';
import axios from 'axios';


function UsersContainer(){

    const [users, setUsers] = useState([]);

    useEffect(() => {
        const storedData = localStorage.getItem("users")
        if (storedData){
            setUsers(JSON.parse(storedData))
        }
        else {
            axios.get("https://dummyjson.com/users")
            .then(res => {
                setUsers(res.data.users)
                localStorage.setItem("users", JSON.stringify(res.data.users))
            })
        }
    },[])



    return (
        <div className={styles.container}>
            {users.map(userInfo => 
            <UserCard 
                firstName={userInfo.firstName}
                lastName={userInfo.lastName}
                image={userInfo.image}
                ip={userInfo.ip}
                key={userInfo.id}
            />)}
            
        </div>
    )
}

export default UsersContainer;