import styles from './Users.module.css'

function UserCard({firstName, lastName, image, ip}){




    return (
        <div className={styles.card}>
            <img src={image} alt="profile" className={styles.photo} />
            <h3 className={styles.name}>{firstName + " " + lastName}</h3>
            <p className={styles.ip}>{ip}</p>
            <button className={styles.btn}>Open Profile</button>
        </div>
    )
}

export default UserCard